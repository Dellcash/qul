FROM node:16
RUN mkdir -p /usr/app
WORKDIR /usr/app
COPY package*.json ./
RUN npm install -g @quasar/cli
RUN npm
COPY . .
