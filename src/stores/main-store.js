import { defineStore } from "pinia";
import axios from "axios";
import { Loading } from "quasar";
// import { api } from "boot/axios";
// import { API_URL_AUTH } from "src/base-url";

export const useMainStore = defineStore("main", {
  state: () => ({
    countries: [],
  }),
  getters: {},
  actions: {
    // async getData() {
    //   try{
    //     const {data} = await api.get(API_URL_AUTH.register)
    //     console.log(data)
    //   }
    //   catch(err){
    //     console.log(err)
    //   }
    // },
    getData() {
      Loading.show();
      axios
        .get("http://tnc2.netall.live/Country/CountryCode", {
          header: {
            "Content-Type": "application/json",
          },
        })
        .then((res) => {
          this.countries = res.data;
          Loading.hide();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  },
});
